<?php
	session_start();
	ob_start();
?>
<html>
	<head>
		<link rel="shortcut icon" href="../assets/image/globe.png" />
		<title>Prasatya-News</title>
		<!--Social Media-->
			<link rel="stylesheet" href="../assets/css/socialmedia/style.css">
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
		<!--Icon Textbox-->
			<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
		<!--Font-->
			<link href="https://fonts.googleapis.com/css?family=Shrikhand|Righteous|Staatliche|Concert+One" rel="stylesheet">
		<!-- Menu -->
			<script type="text/javascript" src="../assets/js/menu_bar/jquery-1.8.3.min.js"></script>
			<script type="text/javascript" src="../assets/js/menu_bar/twd-menu.js"></script>
		<!-- Main -->
			<link rel="stylesheet" type="text/css" href="../assets/css/css-basic.css">
		<!-- Clock -->
			<script type="text/javascript" src="../assets/js/clock/jquery-1.6.4.min.js"></script>
			<link rel="stylesheet" type="text/css" href="../assets/css/clock/clock_css.css">
			<script type="text/javascript" src="../assets/js/clock/clock_js.js"></script>
		<!-- Popup -->
			<script type="text/javascript" src="../assets/js/popup/js_popup.js"></script>
	</head>
	<body>
		<div id = "header">
			<div class= "header_kiri">
				<img src="../assets/image/globe.png" >
				<h1>Prasatya-News</h1>
			</div>
			<div class = "header_kanan">
				<div id="clock_admin">
					<div class="clock">
					<div id="Date"></div>
						<ul>
							<li id="hours"> </li>
							<li id="point">:</li>
							<li id="min"> </li>
							<li id="point">:</li>
							<li id="sec"> </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="bar_menu_admin">
				<nav>
					<ul id="nav">
						<li><a href="../index.php">Home</a></li>
						<li><a href="../about.php">About</a></li>
						<li><a href="#">Type News</a>
							<ul>
								<li><a href="../tech_index.php">Technology</a></li>
								<li><a href="../finc_index.php">Finance</a></li>
								<li><a href="../sport_index.php">Sport</a></li>
							</ul>
						</li>
					</ul>
				</nav>
		</div>
		<div class="mainmenu" >
			<div class="mainmenu_kiri" >
				<?php
					if (isset($_SESSION['username']))
					{
						$user = $_SESSION["username"];						
						echo "
							<img src='aksigambar.php' />
							<p>Welcome <em>$user</em></p>
						";
					}
					else{
						header("location:../login.php");	
						}
				?>
						
				<table>
					<tr>
						<td>
							<div class="button_admin">
								<a href="menunews.php">Menu News
								<i class="fa fa-file-text fa-lg fa-fw" aria-hidden="true"></i></a>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="button_admin">
								<a href="menuuser.php">Menu User
								<i class="fa fa-user fa-lg fa-fw" aria-hidden="true"></i></a>
							</div>
						</td>
					</tr>
					<tr>
						<td>
								<?php
									$user = $_SESSION["username"];
									echo"
										<div class='button_admin'>
											<a onclick='return logout_user(\"$user\")' href='#'>Logout
											<i class='fa fa-sign-out fa-lg fa-fw' aria-hidden='true'></i></a>
										</div>
									";
								?>
						</td>
					</tr>
				</table>
			</div>
			<div class="mainmenu_kanan">
				<div class="isiadmin">
				<div class='hr_main_edit' >
					<p><img src='../assets/image/people.png'/>
					Add User</p>
					<hr width=20% size=7 NOSHADE>
				</div>
					<div class="isi_edituser">
						<form action = "../proses/proses_adduser.php" method="POST" enctype="multipart/form-data">
							<div class='textbox_admin box-icon_admin'>
								<input type='text' name='txtfirstname' placeholder='First Name' autocomplete='off' required='required'/>
								<i class='fa fa-tags fa-lg fa-fw' aria-hidden='true'></i>
							</div>
							
							<div class='textbox_admin box-icon_admin'>
								<input type='text' name='txtlastname' placeholder='Last Name' autocomplete='off' required='required'/>
								<i class='fa fa-tags fa-lg fa-fw' aria-hidden='true'></i>
							</div>
									
							<div class='textbox_admin box-icon_admin'>
								<input type='email' name='txtemail' placeholder='Email' autocomplete='off' required='required'/>
								<i class='fa fa-envelope fa-lg fa-fw' aria-hidden='true'></i>
							</div>
										
							<div class='textbox_admin box-icon_admin'>
								<input type='text' name='txtusername' placeholder='Username' autocomplete='off' required='required'/>
								<i class='fa fa-user fa-lg fa-fw' aria-hidden='true'></i>
							</div>
										
							<div class='textbox_admin box-icon_admin'>
								<input type='password' name='txtpassword' placeholder='Password' autocomplete='off' required='required'/>
								<i class='fa fa-key fa-lg fa-fw' aria-hidden='true'></i>
							</div>
										
							<div class='textbox_admin box-icon_admin'>
								<input type='password' name='txtretypepassword' placeholder='ReTypePassword' autocomplete='off' required='required'/>
								<i class='fa fa-key fa-lg fa-fw' aria-hidden='true'></i>
							</div>
							<div class='textbox_news'>
							<input type='file' name='foto' id='foto' required='required'/>
							</div>
							<div class='textbox_news'>
								<input id='submit' type='submit' name='submit' value='Add' class='button_submit'>
							</div>
						</form>
							<?php
								if(isset($_GET['psn'])){
								$pesan_user = $_GET['psn'];
								echo"
									<script>alert('$pesan_user');</script>
								";
								}
							?>
					</div>
				</div>
			</div>
		</div>
		<div id = "footer">
			<div class="follow">
				<p>Follow Us</p>
			</div>
			<div class="middle">
				  <a class="btn" href="#">
					<i class="fab fa-facebook-f"></i>
				  </a>
				  <a class="btn" href="#">
					<i class="fab fa-twitter"></i>
				  </a>
				  <a class="btn" href="#">
					<i class="fab fa-google"></i>
				  </a>
				  <a class="btn" href="#">
					<i class="fab fa-instagram"></i>
				  </a>
				  <a class="btn" href="#">
					<i class="fab fa-youtube"></i>
				  </a>
			</div>
			<div class="copy_right">
				<p>Copy&copy;PrasatyaNews-201531220@sttpln.ac.id</p>
			</div>
		</div>
	</body>
</html>