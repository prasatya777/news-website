<?php
	include('../koneksi/koneksi.php');
	session_start();
	ob_start();
	if (isset($_SESSION['username']))
	{
		if(isset($_GET['id'])){
			$iduser = $_GET['id'];
			try {
				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				$pdo = $conn->prepare('Delete from tbl_user where id_user = :id');

				$deletedata = array(':id' => $iduser);

				$pdo->execute($deletedata);
				
				if($_SESSION['iduser_login'] == $iduser)
					{
						session_destroy();
					}
					
				header("location:../page_admin/menuuser.php?psn=Delete User Berhasil");

			} catch (PDOexception $e) {
				print "Hapus data gagal: " . $e->getMessage() . "<br/>";
			   die();
			}	

		}
	}
	else{
		header("location:../login.php");	
		}
?>