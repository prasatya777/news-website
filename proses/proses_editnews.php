<?php
	include('../koneksi/koneksi.php');
	session_start();
	ob_start();
	if (isset($_SESSION['username']))
	{
		 if(isset($_POST['submit'])&&isset($_SESSION['id'])){
			$id_news = $_SESSION['id'];
			$title = $_POST['txttitle'];
			$news = $_POST['txtnews'];
			$typenews = $_POST['typenews'];
			$username = $_SESSION['username'];
			date_default_timezone_set('Asia/Jakarta');
			$date=date('y-m-d H:i:s');
			$view=0;
			$imagecek = $_FILES['foto']['name'];
			$check = getimagesize($_FILES["foto"]["tmp_name"]);

					if($check !== false) {
						$name = $_FILES['foto']['name'];
						$type = $_FILES['foto']['type'];
						$data = file_get_contents($_FILES['foto']['tmp_name']);
						try {				
								$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
								$pdo = $conn->prepare('UPDATE tbl_news 
															set 
															title =:judul, 
															news =:berita,
															type_news =:tipeberita,
															date_news =:tglberita,
															user =:pengguna,
															image_news =:gambar,
															image_name_news =:nama_gambar, 
															image_mime_type_news =:tipe_gambar
															where id_news = :id');

								$updatedata = array(':judul' => $title, ':berita' => $news, ':tipeberita' => $typenews,
										':tglberita' => $date, ':pengguna' => $username,
										':gambar' => $data, ':nama_gambar' => $name, ':tipe_gambar' => $type, ':id' => $id_news);

								$pdo->execute($updatedata);
								header("location:../page_admin/menunews.php?psn=Update Berita Berhasil");

							} catch (PDOexception $e) {
								print "Update data gagal: " . $e->getMessage() . "<br/>";
							   die();
							}	
					} 
					else if(empty($imagecek))
					{
						try {				
								$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
								$pdo = $conn->prepare('UPDATE tbl_news 
															set 
															title =:judul, 
															news =:berita,
															type_news =:tipeberita,
															date_news =:tglberita,
															user =:pengguna
															where id_news = :id');

								$updatedata = array(':judul' => $title, ':berita' => $news, ':tipeberita' => $typenews,
										':tglberita' => $date, ':pengguna' => $username,':id' => $id_news);

								$pdo->execute($updatedata);
								header("location:../page_admin/menunews.php?psn=Update Berita Berhasil");

							} catch (PDOexception $e) {
								print "Update data gagal: " . $e->getMessage() . "<br/>";
							   die();
							}
					}
					else 
						{
							header("location:../page_admin/edit_news.php?psn=Maaf Format Foto Tidak Sesuai&id=$id_news");
						}
		}
	}
	else{
		header("location:../login.php");	
		}
?>