<?php
	include('../koneksi/koneksi.php');
	session_start();
	ob_start();
	if (isset($_SESSION['username']))
	{
		if(isset($_GET['id'])){
			$idnews = $_GET['id'];
			try {
				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				$pdo = $conn->prepare('Delete from tbl_news where id_news = :id');

				$deletedata = array(':id' => $idnews);

				$pdo->execute($deletedata);
				header("location:../page_admin/menunews.php?psn=Delete Berita Berhasil");

			} catch (PDOexception $e) {
				print "Hapus data gagal: " . $e->getMessage() . "<br/>";
			   die();
			}	

		}
	}
	else{
		header("location:../login.php");	
		}
?>