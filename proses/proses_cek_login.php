<?php
include '../koneksi/koneksi.php';
session_start();
ob_start();
If(isset($_POST['submit'])){
	$user = $_POST['txtusername'];
	$pwd = $_POST['txtpassword'];

	$pdo = $conn->prepare('SELECT * FROM tbl_user where username = :user and password = :pwd');

	$pdo->execute(array(':user' => $user, ':pwd' => $pwd));

	$count = $pdo->rowcount();
	$row= $pdo->fetch(PDO::FETCH_OBJ);
	
	if($count==0){
		header("location:../login.php?psn=Maaf Password atau Username Salah");
	}else{
		$_SESSION['iduser_login'] = $row->id_user;
		$_SESSION['username'] = $user;
		$_SESSION['image'] = $row->image;
		$_SESSION['image_name'] = $row->image_name;
		$_SESSION['image_mime_type'] = $row->image_mime_type;
		
		header("location:../page_admin/mainmenu.php");
	}
}
else{
	header("location:../login.php");
	}
?>