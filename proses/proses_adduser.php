<?php
	include '../koneksi/koneksi.php';
	session_start();
	ob_start();
	if (isset($_SESSION['username']))
	{
		if(isset($_POST['submit'])){
			$firstname = $_POST['txtfirstname'];
			$lastname = $_POST['txtlastname'];
			$email = $_POST['txtemail'];
			$username = $_POST['txtusername'];
			$password = $_POST['txtpassword'];
			$r_password = $_POST['txtretypepassword'];
			if ($password<>$r_password)
				{
						header("location:../register.php?psn=Maaf Password Yang Anda Masukkan Tidak Sama");
				}
			else
				{		
					// Check if image file is a actual image or fake image
					$check = getimagesize($_FILES["foto"]["tmp_name"]);
					if($check == false) {
						header("location:../page_admin/add_user.php?psn=Maaf Format Foto Tidak Sesuai");
					} 
					else 
					{
						$name = $_FILES['foto']['name'];
						$type = $_FILES['foto']['type'];
						$data = file_get_contents($_FILES['foto']['tmp_name']);
						try {
							$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
							$pdo = $conn->prepare('INSERT INTO tbl_user (firstname, lastname, email, username, password, image, image_name, image_mime_type) 
													values (:nm_depan, :nm_belakang, :mail, :pengguna, :sandi, :gambar, :nama_gambar, :tipe_gambar)');
							$insertdata = array(':nm_depan' => $firstname, ':nm_belakang' => $lastname, ':mail' => $email,
										':pengguna' => $username, ':sandi' => $password,
										':gambar' => $data, ':nama_gambar' => $name, ':tipe_gambar' => $type);
							$pdo->execute($insertdata);
						
								header("location:../page_admin/menuuser.php?psn=Tambah User Berhasil");
							
						} catch (PDOexception $e) {
							print "Insert data gagal: " . $e->getMessage() . "<br/>";
						   die();
						}
					}

				}
		}
	}
	else{
	header("location:../login.php");	
	}
?>