<?php
	include('../koneksi/koneksi.php');
	ob_start();
		if(isset($_GET['idnews'])){
			$idnews = $_GET['idnews'];
		try
		{		
			$pdo = $conn->prepare('SELECT view FROM tbl_news where id_news = :id');
			$pdo->bindparam(':id', $idnews);
			$pdo->execute();
			$row= $pdo->fetch(PDO::FETCH_OBJ);
			$view=$row->view;
		} catch (PDOexception $e) {
		print "Cari data gagal: " . $e->getMessage() . "<br/>";
		die();
		}
		
		$view=$view+1;
		
		try {
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('UPDATE tbl_news 
										set view = :vi
										where id_news = :id');
				$updatedata = array(':vi'=>$view,':id' => $idnews);
				$pdo->execute($updatedata);
				header("location:../detail_news.php?id_ns=$idnews");
			} catch (PDOexception $e) {
			print "Update data gagal: " . $e->getMessage() . "<br/>";
			die();
			}	
					
		}
?>