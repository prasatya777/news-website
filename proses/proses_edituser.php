<?php
	include('../koneksi/koneksi.php');
	session_start();
	ob_start();
	if (isset($_SESSION['username']))
	{
		 if(isset($_POST['submit'])&&isset($_SESSION['id'])){
			$id_user = $_SESSION['id'];
			$firstname = $_POST['txtfirstname'];
			$lastname = $_POST['txtlastname'];
			$email = $_POST['txtemail'];
			$username = $_POST['txtusername'];
			$password = $_POST['txtpassword'];
			$r_password = $_POST['txtretypepassword'];
			$imagecek = $_FILES['foto']['name'];
			$check = getimagesize($_FILES["foto"]["tmp_name"]);

			if ($password<>$r_password)
				{
				header("location:../page_admin/edit_user.php?psn=Maaf Password Yang Anda Masukkan Tidak Sama&id=$id_user");
				}
			else
				{
					if($check !== false) {
						$name = $_FILES['foto']['name'];
						$type = $_FILES['foto']['type'];
						$data = file_get_contents($_FILES['foto']['tmp_name']);
						try {				
								$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
								$pdo = $conn->prepare('UPDATE tbl_user 
															set 
															firstname =:nm_depan, 
															lastname =:nm_belakang,
															email =:mail,
															username =:pengguna,
															password =:sandi,
															image =:gambar, 
															image_name =:nama_gambar, 
															image_mime_type =:tipe_gambar
															where id_user = :id');

								$updatedata = array(':nm_depan' => $firstname, ':nm_belakang' => $lastname, ':mail' => $email,
										':pengguna' => $username, ':sandi' => $password,
										':gambar' => $data, ':nama_gambar' => $name, ':tipe_gambar' => $type, ':id' => $id_user);
								$pdo->execute($updatedata);
								if($_SESSION['iduser_login'] == $id_user)
								{
									$_SESSION['image'] = $data;
									$_SESSION['image_name'] = $name;
									$_SESSION['image_mime_type'] = $type;
								}
								header("location:../page_admin/menuuser.php?psn=Update Username : $username dengan ID : $id_user Berhasil");

							} catch (PDOexception $e) {
								print "Update data gagal: " . $e->getMessage() . "<br/>";
							   die();
							}	
					} 
					elseif(empty($imagecek))
					{
						try {				
								$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
								$pdo = $conn->prepare('UPDATE tbl_user 
															set 
															firstname =:nm_depan, 
															lastname =:nm_belakang,
															email =:mail,
															username =:pengguna,
															password =:sandi
															where id_user = :id');

								$updatedata = array(':nm_depan' => $firstname, ':nm_belakang' => $lastname, ':mail' => $email,
										':pengguna' => $username, ':sandi' => $password, ':id' => $id_user);
								$pdo->execute($updatedata);

								header("location:../page_admin/menuuser.php?psn=Update Username : $username dengan ID : $id_user Berhasil");

							} catch (PDOexception $e) {
								print "Update data gagal: " . $e->getMessage() . "<br/>";
							   die();
							}

					}
					else 
						{
							header("location:../page_admin/edit_user.php?psn=Maaf Format Foto Tidak Sesuai&id=$id_user");
						}
			
				}
		}
	}
	else{
		header("location:../login.php");	
		}
?>