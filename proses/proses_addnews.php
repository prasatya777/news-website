<?php
	include '../koneksi/koneksi.php';
	session_start();
	ob_start();
	if (isset($_SESSION['username']))
	{
		if(isset($_POST['submit'])){
			$title = $_POST['txttitle'];
			$news = $_POST['txtnews'];
			$typenews = $_POST['typenews'];
			$username = $_SESSION['username'];
			date_default_timezone_set('Asia/Jakarta');
			$date=date('y-m-d H:i:s');
			$view=0;
					// Check if image file is a actual image or fake image
					$check = getimagesize($_FILES["foto"]["tmp_name"]);
					if($check == false) {
						header("location:../page_admin/add_news.php?psn=Maaf Format Foto Tidak Sesuai");
					} 
					else 
					{
						$name = $_FILES['foto']['name'];
						$type = $_FILES['foto']['type'];
						$data = file_get_contents($_FILES['foto']['tmp_name']);
						try {
							$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
							$pdo = $conn->prepare('INSERT INTO tbl_news (title, news, type_news, date_news, user, view, image_news, image_name_news, image_mime_type_news) 
													values (:judul, :berita, :tipeberita, :tglberita, :pengguna, :tayang, :gambar, :nama_gambar, :tipe_gambar)');
							$insertdata = array(':judul' => $title, ':berita' => $news, ':tipeberita' => $typenews,
										':tglberita' => $date, ':pengguna' => $username, ':tayang' => $view,
										':gambar' => $data, ':nama_gambar' => $name, ':tipe_gambar' => $type);
							$pdo->execute($insertdata);
						
								header("location:../page_admin/menunews.php?psn=Tambah Berita Berhasil");
							
						} catch (PDOexception $e) {
							print "Insert data gagal: " . $e->getMessage() . "<br/>";
						   die();
						}
					}
		}
	}
	else{
	header("location:../login.php");	
	}
?>