<?php
	include "koneksi/koneksi.php";
	session_start();
	ob_start();
?>

<!------------------------------------------ Script CSS dan PHP Slide Image -------------------------------------------------->
<?php
	$i=1;
	$result = $conn->query('select * from tbl_news order by view DESC');
	echo"<style type='text/css'>";
	while($row=$result->fetch(PDO::FETCH_OBJ)){
		$isiberita = substr($row->news,0,500);
		if($i==1)
			{
				echo"
				.slide:nth-child($i){left:0}
				.slide:nth-child($i) 
				.slide__bg{left:0;background-image:url('imagenews.php?id=$row->id_news')}.slide:nth-child($i) 
				.slide__overlay-path{fill:#e99c7e}
				";
			}
		else if($i==2)
			{
				echo"
				.slide:nth-child($i){left:100%}
				.slide:nth-child($i) 
				.slide__bg{left:-50%;background-image:url('imagenews.php?id=$row->id_news')}.slide:nth-child($i) 
				.slide__overlay-path{fill:#e1ccae}
				";
			}
		else if($i==3)
			{
				echo"
				.slide:nth-child($i){left:200%}
				.slide:nth-child($i) 
				.slide__bg{left:-100%;background-image:url('imagenews.php?id=$row->id_news')}.slide:nth-child($i) 
				.slide__overlay-path{fill:#adc5cd}
				";
			}
		else if($i==4)
			{
				echo"
				.slide:nth-child($i){left:300%}
				.slide:nth-child($i) 
				.slide__bg{left:-150%;background-image:url('imagenews.php?id=$row->id_news')}.slide__content,.slide__overlay{left:0;height:100%;position:absolute}.slide:nth-child($i) 
				.slide__overlay-path{fill:#cbc6c3}
				";
			}
		$i++;
	}
	echo"</style>";
?>
<!---------------------------------------------------------------------------------------------------------------------------->

<html>
	<head>
		<link rel="shortcut icon" href="assets/image/globe.png" />
		<title>Prasatya-News</title>
		<!--Social Media-->
			<link rel="stylesheet" href="assets/css/socialmedia/style.css">
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
		<!--Icon Textbox-->
			<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
		<!--Font-->
			<link href="https://fonts.googleapis.com/css?family=Shrikhand|Righteous|Staatliche|Concert+One" rel="stylesheet">
		<!-- Menu -->
			<script type="text/javascript" src="assets/js/menu_bar/jquery-1.8.3.min.js"></script>
			<script type="text/javascript" src="assets/js/menu_bar/twd-menu.js"></script>
		<!-- Slide Image-->
			<link rel="stylesheet" href="assets/css/slideimage/styles.min.css">
		<!-- Main -->
			<link rel="stylesheet" type="text/css" href="assets/css/css-basic.css">
		<!-- Clock -->
			<script type="text/javascript" src="assets/js/clock/jquery-1.6.4.min.js"></script>
			<link rel="stylesheet" type="text/css" href="assets/css/clock/clock_css.css">
			<script type="text/javascript" src="assets/js/clock/clock_js.js"></script>
		<!-- Popup -->
			<script type="text/javascript" src="assets/js/popup/js_popup.js"></script>
	</head>
	<body>
		<div id = "header">
			<div class= "header_kiri">
				<img src="assets/image/globe.png" >
				<h1>Prasatya-News</h1>
			</div>
			<div class = "header_kanan">
				<form action = "result_search.php" method="POST" enctype="multipart/form-data">
					<div class="search-textbox box-icon">
						<input type="text" name="search[keyword]" placeholder="Searching" required="required"/>
						<i class="fa fa-search fa-lg fa-fw" aria-hidden="true"></i>
					</div>
				</form>
				<?php
					if (isset($_SESSION['username']))
					{
						$user=$_SESSION['username'];
						echo"
								<p> Welcome <em><strong>$user</strong></em> | <a href='page_admin/mainmenu.php'>Admin Panel</a> | <a onclick='return logout_user_frontend(\"$user\")' href='#'>Logout</a></p>
						";
					}
					else{
						echo"
								<p><a href='login.php'>Login</a> | <a href='register.php'>Register</a></p>
						";
						}
				?>
			</div>
		</div>
		<div class="bar_menu">
				<nav>
					<ul id="nav">
						<li><a href="index.php">Home</a></li>
						<li><a href="about.php">About</a></li>
						<li><a href="#">Type News</a>
							<ul>
								<li><a href="tech_index.php">Technology</a></li>
								<li><a href="finc_index.php">Finance</a></li>
								<li><a href="sport_index.php">Sport</a></li>
							</ul>
						</li>
					</ul>
				</nav>
		</div>
		<div id = "menu">
			<div id = "slideimage">
				<div class="slider-container">
					<div class="slider-control left inactive"></div>
					<div class="slider-control right"></div>
					<ul class="slider-pagi"></ul>
					<div class="slider">
					  <?php
						$i=0;
						$result = $conn->query('select * from tbl_news order by view DESC');
						while($row=$result->fetch(PDO::FETCH_OBJ)){
							$isiberita = substr($row->news,0,200);
								if($i==0)
								{
								echo "
									<div class='slide slide-$i active'>
										<div class='slide__bg'></div>
											<div class='slide__content'>
												<svg class='slide__overlay' viewBox='0 0 300 405' preserveAspectRatio='xMaxYMax slice'>
													<path class='slide__overlay-path' d='M0,0 145,0 280,405 0,405' />
												</svg>
												<div class='slide__text'>
													<h2 class='slide__text-heading'>$row->title</h2>
													<p class='slide__text-desc'>$isiberita</p>
													<a onclick='return count_view(\"$row->id_news\")' href='#' class='slide__text-link'><strong><em>Readmore</em></strong></a>
												</div>
											</div>
									</div>
									";
								}
								else if($i<=3)
								{
								echo"
									<div class='slide slide-$i '>
										<div class='slide__bg'></div>
											<div class='slide__content'>
												<svg class='slide__overlay' viewBox='0 0 300 405' preserveAspectRatio='xMaxYMax slice'>
													<path class='slide__overlay-path' d='M0,0 145,0 280,405 0,405' />
												</svg>
												<div class='slide__text'>
													<h2 class='slide__text-heading'>$row->title</h2>
														<p class='slide__text-desc'>$isiberita</p>
														<a onclick='return count_view(\"$row->id_news\")' href='#' class='slide__text-link'><strong><em>Readmore</em></strong></a>
												</div>
											</div>
										
									</div>
									";
								}
							$i++;
						}
						?>
					</div>
				</div>
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
					<script src="assets/js/slideimage/script.min.js"></script>
			</div>
		</div>
		<div id = "articlehome">
			<div class = "kiri-articlehome">
				<div class="hr_home_kiri" >
					<p>News</p>
					<hr width=15% size=7 NOSHADE>
				</div>
				<div class="article_main img_detail">
					  <?php
						$idnews=$_GET['id_ns'];
						$result = $conn->query('select * from tbl_news where id_news = '.$idnews.'');
						while($row=$result->fetch(PDO::FETCH_OBJ)){
							$isiberita = $row->news;
						echo "
							<table>
							<tr>
								<td><h2>$row->title</h2></td>
							</tr>
							<tr>
								<td>
									<div class='publish'>
										<p>Publish by : $row->user | $row->date_news | Type : $row->type_news</p>
										<hr width=100%>
									</div>
								<td>
							</tr>
							<tr>
								<td>
								<p><img src='imagenews.php?id=$row->id_news'>
								$isiberita</p>
								</td>
							</tr>
							</table>
							";
						}
						?>
				</div>
			</div>
			
			<div class = "kanan-articlehome">
				<div class="hr_home_kanan" >
					<p>Clock</p>
					<hr width=40% size=7 NOSHADE>
				</div>
				<div id="clock">
					<div class="clock">
					<div id="Date"></div>
						<ul>
							<li id="hours"> </li>
							<li id="point">:</li>
							<li id="min"> </li>
							<li id="point">:</li>
							<li id="sec"> </li>
						</ul>
					</div>
				</div>
				<div class="hr_home_kanan" >
					<p>Populer News</p>
					<hr width=48% size=7 NOSHADE>
				</div>
					<div class="populer_news">
					  <?php
						$i=1;
						echo"
							<table>
						";
						$result = $conn->query('select id_news, title from tbl_news order by view DESC');
						while($row=$result->fetch(PDO::FETCH_OBJ)){
						if($i<=10)
						{
							echo "
								<tr>
									<td class='number' width=15%><p>$i</p></td>
									<td class='title_news'><p><a onclick='return count_view(\"$row->id_news\")' href='#'>$row->title</a></p></td>
								</tr>
							";
						}
						$i++;
						}
						echo"
							</table>
						";
						?>
					</div>
			</div>
		</div>
		<div id = "footer">
			<div class="follow">
				<p>Follow Us</p>
			</div>
			<div class="middle">
				  <a class="btn" href="#">
					<i class="fab fa-facebook-f"></i>
				  </a>
				  <a class="btn" href="#">
					<i class="fab fa-twitter"></i>
				  </a>
				  <a class="btn" href="#">
					<i class="fab fa-google"></i>
				  </a>
				  <a class="btn" href="#">
					<i class="fab fa-instagram"></i>
				  </a>
				  <a class="btn" href="#">
					<i class="fab fa-youtube"></i>
				  </a>
			</div>
			<div class="copy_right">
				<p>Copy&copy;PrasatyaNews-201531220@sttpln.ac.id</p>
			</div>
		</div>
	</body>
</html>