<?php
	include "koneksi/koneksi.php";
	session_start();
	ob_start();
?>

<!------------------------------------------ Script CSS dan PHP Slide Image -------------------------------------------------->
<?php
	$i=1;
	$result = $conn->query('select * from tbl_news order by view DESC');
	echo"<style type='text/css'>";
	while($row=$result->fetch(PDO::FETCH_OBJ)){
		$isiberita = substr($row->news,0,500);
		if($i==1)
			{
				echo"
				.slide:nth-child($i){left:0}
				.slide:nth-child($i) 
				.slide__bg{left:0;background-image:url('imagenews.php?id=$row->id_news')}.slide:nth-child($i) 
				.slide__overlay-path{fill:#e99c7e}
				";
			}
		else if($i==2)
			{
				echo"
				.slide:nth-child($i){left:100%}
				.slide:nth-child($i) 
				.slide__bg{left:-50%;background-image:url('imagenews.php?id=$row->id_news')}.slide:nth-child($i) 
				.slide__overlay-path{fill:#e1ccae}
				";
			}
		else if($i==3)
			{
				echo"
				.slide:nth-child($i){left:200%}
				.slide:nth-child($i) 
				.slide__bg{left:-100%;background-image:url('imagenews.php?id=$row->id_news')}.slide:nth-child($i) 
				.slide__overlay-path{fill:#adc5cd}
				";
			}
		else if($i==4)
			{
				echo"
				.slide:nth-child($i){left:300%}
				.slide:nth-child($i) 
				.slide__bg{left:-150%;background-image:url('imagenews.php?id=$row->id_news')}.slide__content,.slide__overlay{left:0;height:100%;position:absolute}.slide:nth-child($i) 
				.slide__overlay-path{fill:#cbc6c3}
				";
			}
		$i++;
	}
	echo"</style>";
?>
<!---------------------------------------------------------------------------------------------------------------------------->

<html>
	<head>
		<link rel="shortcut icon" href="assets/image/globe.png" />
		<title>Prasatya-News</title>
		<!--Social Media-->
			<link rel="stylesheet" href="assets/css/socialmedia/style.css">
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
		<!--Icon Textbox-->
			<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
		<!--Font-->
			<link href="https://fonts.googleapis.com/css?family=Shrikhand|Righteous|Staatliche|Concert+One" rel="stylesheet">
		<!-- Menu -->
			<script type="text/javascript" src="assets/js/menu_bar/jquery-1.8.3.min.js"></script>
			<script type="text/javascript" src="assets/js/menu_bar/twd-menu.js"></script>
		<!-- Slide Image-->
			<link rel="stylesheet" href="assets/css/slideimage/styles.min.css">
		<!-- Main -->
			<link rel="stylesheet" type="text/css" href="assets/css/css-basic.css">
		<!-- Page Number -->
			<link rel="stylesheet" type="text/css" href="assets/css/pagnitation/css_pagnitation.css">
		<!-- Clock -->
			<script type="text/javascript" src="assets/js/clock/jquery-1.6.4.min.js"></script>
			<link rel="stylesheet" type="text/css" href="assets/css/clock/clock_css.css">
			<script type="text/javascript" src="assets/js/clock/clock_js.js"></script>
		<!-- Popup -->
			<script type="text/javascript" src="assets/js/popup/js_popup.js"></script>
	</head>
	<body>
		<div id = "header">
			<div class= "header_kiri">
				<img src="assets/image/globe.png" >
				<h1>Prasatya-News</h1>
			</div>
			<div class = "header_kanan">
				<form action = "result_search.php" method="POST" enctype="multipart/form-data">
					<div class="search-textbox box-icon">
						<input type="text" name="search[keyword]" placeholder="Searching" required="required"/>
						<i class="fa fa-search fa-lg fa-fw" aria-hidden="true"></i>
					</div>
				</form>
				<?php
					if (isset($_SESSION['username']))
					{
						$user=$_SESSION['username'];
						echo"
								<p> Welcome <em><strong>$user</strong></em> | <a href='page_admin/mainmenu.php'>Admin Panel</a> | <a onclick='return logout_user_frontend(\"$user\")' href='#'>Logout</a></p>
						";
					}
					else{
						echo"
								<p><a href='login.php'>Login</a> | <a href='register.php'>Register</a></p>
						";
						}
				?>
			</div>
		</div>
		<div class="bar_menu">
				<nav>
					<ul id="nav">
						<li><a href="index.php">Home</a></li>
						<li><a href="about.php">About</a></li>
						<li><a href="#">Type News</a>
							<ul>
								<li><a href="tech_index.php">Technology</a></li>
								<li><a href="finc_index.php">Finance</a></li>
								<li><a href="sport_index.php">Sport</a></li>
							</ul>
						</li>
					</ul>
				</nav>
		</div>
		<div id = "menu">
			<div id = "slideimage">
				<div class="slider-container">
					<div class="slider-control left inactive"></div>
					<div class="slider-control right"></div>
					<ul class="slider-pagi"></ul>
					<div class="slider">
					  <?php
						$i=0;
						$result = $conn->query('select * from tbl_news order by view DESC');
						while($row=$result->fetch(PDO::FETCH_OBJ)){
							$isiberita = substr($row->news,0,200);
								if($i==0)
								{
								echo "
									<div class='slide slide-$i active'>
										<div class='slide__bg'></div>
											<div class='slide__content'>
												<svg class='slide__overlay' viewBox='0 0 300 405' preserveAspectRatio='xMaxYMax slice'>
													<path class='slide__overlay-path' d='M0,0 145,0 280,405 0,405' />
												</svg>
												<div class='slide__text'>
													<h2 class='slide__text-heading'>$row->title</h2>
													<p class='slide__text-desc'>$isiberita</p>
													<a onclick='return count_view(\"$row->id_news\")' href='#' class='slide__text-link'><strong><em>Readmore</em></strong></a>
												</div>
											</div>
									</div>
									";
								}
								else if($i<=3)
								{
								echo"
									<div class='slide slide-$i '>
										<div class='slide__bg'></div>
											<div class='slide__content'>
												<svg class='slide__overlay' viewBox='0 0 300 405' preserveAspectRatio='xMaxYMax slice'>
													<path class='slide__overlay-path' d='M0,0 145,0 280,405 0,405' />
												</svg>
												<div class='slide__text'>
													<h2 class='slide__text-heading'>$row->title</h2>
														<p class='slide__text-desc'>$isiberita</p>
														<a onclick='return count_view(\"$row->id_news\")' href='#' class='slide__text-link'><strong><em>Readmore</em></strong></a>
												</div>
											</div>
										
									</div>
									";
								}
							$i++;
						}
						?>
					</div>
				</div>
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
					<script src="assets/js/slideimage/script.min.js"></script>
			</div>
		</div>
		<div id = "articlehome">
			<div class = "kiri-articlehome">
				<div class="hr_home_kiri" >
					<p>Update News (Technology)</p>
					<hr width=55% size=7 NOSHADE>
				</div>
				<div class="article_main">
					  <?php
						//Menampilkan Jumlah Halaman Yang Ingin Ditampilkan
						$page = (isset($_GET['page']))? $_GET['page'] : 1;
						$limit = 5;
						$limit_start = ($page - 1) * $limit;
						$result = $conn->prepare('SELECT * FROM tbl_news where type_news="Technology" order by date_news DESC LIMIT '.$limit_start.','.$limit.'');
						$result->execute();
						while($row=$result->fetch(PDO::FETCH_OBJ)){
							$isiberita = substr($row->news,0,500);
						echo "
							<table>
							<tr>
								<td><h2>$row->title</h2></td>
							</tr>
							<tr>
								<td>
									<div class='publish'>
										<p>Publish by : $row->user | $row->date_news | Type : $row->type_news</p>
										<hr width=100%>
									</div>
								<td>
							</tr>
							<tr>
								<td>
								<p><img src='imagenews.php?id=$row->id_news'>
								$isiberita <a onclick='return count_view(\"$row->id_news\")' href='#'><strong><em>Readmore</em></strong></a></p>
								</td>
							</tr>
							</table>
							";
						}
						?>
<!-------------------------------- Penomoran Page ------------------------------------------------------------------>
					<div class = "pagenitaion">
					  <ul class="pagination">
						<?php
						if($page == 1){ // Jika page adalah page ke 1, maka disable link PREV
						echo"
						  <li class='disabled'><a href='#'>First</a></li>
						  <li class='disabled'><a href='#'>&laquo;</a></li>
						  ";
						}else{ // Jika page bukan page ke 1
						  $link_prev = ($page > 1)? $page - 1 : 1;
						echo"
						  <li><a href='tech_index.php?page=1'>First</a></li>
						  <li><a href='tech_index.php?page=$link_prev'>&laquo;</a></li>
							";
						}
						
						/*<!------------------- LINK NUMBER --------------------------------------------------------->*/
						// Buat query untuk menghitung semua jumlah data
						$sql2 = $conn->prepare('SELECT COUNT(*) AS jumlah FROM tbl_news where type_news="Technology"');
						$sql2->execute(); // Eksekusi querynya
						$get_jumlah = $sql2->fetch();
						
						$jumlah_page = ceil($get_jumlah['jumlah'] / $limit); // Hitung jumlah halamannya
						$jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
						$start_number = ($page > $jumlah_number)? $page - $jumlah_number : 1; // Untuk awal link number
						$end_number = ($page < ($jumlah_page - $jumlah_number))? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number
						
						for($i = $start_number; $i <= $end_number; $i++){
						  $link_active = ($page == $i)? ' class="active"' : '';
						echo"
							<li $link_active ><a href='tech_index.php?page=$i'>$i</a></li>
							";
						}
						
						/*<!------------------- LINK NEXT AND LAST -------------------------------------------------->*/
						// Jika page sama dengan jumlah page, maka disable link NEXT nya
						// Artinya page tersebut adalah page terakhir 
						if($page == $jumlah_page){ // Jika page terakhir
						echo"
						  <li class='disabled'><a href='#'>&raquo;</a></li>
						  <li class='disabled'><a href='#'>Last</a></li>
							";
						}else{ // Jika Bukan page terakhir
						  $link_next = ($page < $jumlah_page)? $page + 1 : $jumlah_page;
						echo"
						  <li><a href='tech_index.php?page=$link_next'>&raquo;</a></li>
						  <li><a href='tech_index.php?page=$jumlah_page'>Last</a></li>
							";
						}
						?>
					  </ul>
					</div>
<!---------------------------------------------------------------------------------------------------------------------->
				</div>
			</div>
			
			<div class = "kanan-articlehome">
				<div class="hr_home_kanan" >
					<p>Clock</p>
					<hr width=40% size=7 NOSHADE>
				</div>
				<div id="clock">
					<div class="clock">
					<div id="Date"></div>
						<ul>
							<li id="hours"> </li>
							<li id="point">:</li>
							<li id="min"> </li>
							<li id="point">:</li>
							<li id="sec"> </li>
						</ul>
					</div>
				</div>
				<div class="hr_home_kanan" >
					<p>Populer News (Technology)</p>
					<hr width=85% size=7 NOSHADE>
				</div>
					<div class="populer_news">
					  <?php
						$i=1;
						echo"
							<table>
						";
						$result = $conn->query('select id_news, title from tbl_news where type_news="Technology" order by view DESC');
						while($row=$result->fetch(PDO::FETCH_OBJ)){
						if($i<=10)
						{
							echo "
								<tr>
									<td class='number' width=15%><p>$i</p></td>
									<td class='title_news'><p><a onclick='return count_view(\"$row->id_news\")' href='#'>$row->title</a></p></td>
								</tr>
							";
						}
						$i++;
						}
						echo"
							</table>
						";
						?>
					</div>
			</div>
		</div>
		<div id = "footer">
			<div class="follow">
				<p>Follow Us</p>
			</div>
			<div class="middle">
				  <a class="btn" href="#">
					<i class="fab fa-facebook-f"></i>
				  </a>
				  <a class="btn" href="#">
					<i class="fab fa-twitter"></i>
				  </a>
				  <a class="btn" href="#">
					<i class="fab fa-google"></i>
				  </a>
				  <a class="btn" href="#">
					<i class="fab fa-instagram"></i>
				  </a>
				  <a class="btn" href="#">
					<i class="fab fa-youtube"></i>
				  </a>
			</div>
			<div class="copy_right">
				<p>Copy&copy;PrasatyaNews-201531220@sttpln.ac.id</p>
			</div>
		</div>
	</body>
</html>