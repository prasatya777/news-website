function deleteuser(no,iduser,username)
{
if(confirm("Apakah Anda Yakin Menghapus Username : '"+username+"' Dengan ID : '"+iduser+"' Pada No : '"+no+"' ?"))
	{
		window.location.href='../proses/proses_deleteuser.php?id='+iduser+'';
		return true;
	}
else return false;
}

function updateuser(username)
{
if(confirm("Apakah Anda Yakin Update Data User : '"+username+"' ?"))
	{
		return true;
	}
	else{ 
		return false;
	}
}

function updatenews(title)
{
if(confirm("Apakah Anda Yakin Update Berita Berjudul : '"+title+"' ?"))
	{
		return true;
	}
	else{ 
		return false;
	}
}

function deletenews(no,idnews,title)
{
if(confirm("Apakah Anda Yakin Menghapus Berita : '"+title+"' Pada No : '"+no+"' ?"))
	{
		window.location.href='../proses/proses_deletenews.php?id='+idnews+'';
		return true;
	}
else return false;
}

function logout_user(username)
{
if(confirm("Apakah Anda Yakin Keluar '"+username+"' ?"))
	{
		window.location.href='../proses/proses_logout.php';
		return true;
	}
else return false;
}

function logout_user_frontend(username)
{
if(confirm("Apakah Anda Yakin Keluar '"+username+"' ?"))
	{
		window.location.href='proses/proses_logout.php';
		return true;
	}
else return false;
}

function count_view(id)
{
	window.location.href='proses/proses_view.php?idnews='+id+'';
}