<?php
	include "koneksi/koneksi.php";
	session_start();
	ob_start();
?>

<!------------------------------------------ Script CSS dan PHP Slide Image -------------------------------------------------->
<?php
	$i=1;
	$result = $conn->query('select * from tbl_news order by view DESC');
	echo"<style type='text/css'>";
	while($row=$result->fetch(PDO::FETCH_OBJ)){
		$isiberita = substr($row->news,0,500);
		if($i==1)
			{
				echo"
				.slide:nth-child($i){left:0}
				.slide:nth-child($i) 
				.slide__bg{left:0;background-image:url('imagenews.php?id=$row->id_news')}.slide:nth-child($i) 
				.slide__overlay-path{fill:#e99c7e}
				";
			}
		else if($i==2)
			{
				echo"
				.slide:nth-child($i){left:100%}
				.slide:nth-child($i) 
				.slide__bg{left:-50%;background-image:url('imagenews.php?id=$row->id_news')}.slide:nth-child($i) 
				.slide__overlay-path{fill:#e1ccae}
				";
			}
		else if($i==3)
			{
				echo"
				.slide:nth-child($i){left:200%}
				.slide:nth-child($i) 
				.slide__bg{left:-100%;background-image:url('imagenews.php?id=$row->id_news')}.slide:nth-child($i) 
				.slide__overlay-path{fill:#adc5cd}
				";
			}
		else if($i==4)
			{
				echo"
				.slide:nth-child($i){left:300%}
				.slide:nth-child($i) 
				.slide__bg{left:-150%;background-image:url('imagenews.php?id=$row->id_news')}.slide__content,.slide__overlay{left:0;height:100%;position:absolute}.slide:nth-child($i) 
				.slide__overlay-path{fill:#cbc6c3}
				";
			}
		$i++;
	}
	echo"</style>";
?>
<!---------------------------------------------------------------------------------------------------------------------------->

<html>
	<head>
		<link rel="shortcut icon" href="assets/image/globe.png" />
		<title>Prasatya-News</title>
		<!--Social Media-->
			<link rel="stylesheet" href="assets/css/socialmedia/style.css">
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
		<!--Icon Textbox-->
			<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
		<!--Font-->
			<link href="https://fonts.googleapis.com/css?family=Shrikhand|Righteous|Staatliche|Concert+One" rel="stylesheet">
		<!-- Menu -->
			<script type="text/javascript" src="assets/js/menu_bar/jquery-1.8.3.min.js"></script>
			<script type="text/javascript" src="assets/js/menu_bar/twd-menu.js"></script>
		<!-- Main -->
			<link rel="stylesheet" type="text/css" href="assets/css/css-basic.css">
		<!-- Clock -->
			<script type="text/javascript" src="assets/js/clock/jquery-1.6.4.min.js"></script>
			<link rel="stylesheet" type="text/css" href="assets/css/clock/clock_css.css">
			<script type="text/javascript" src="assets/js/clock/clock_js.js"></script>
		<!-- Popup -->
			<script type="text/javascript" src="assets/js/popup/js_popup.js"></script>
	</head>
	<body>
		<div id = "header">
			<div class= "header_kiri">
				<img src="assets/image/globe.png" >
				<h1>Prasatya-News</h1>
			</div>
			<div class = "header_kanan">
				<form action = "result_search.php" method="POST" enctype="multipart/form-data">
					<div class="search-textbox box-icon">
						<input type="text" name="search[keyword]" placeholder="Searching" required="required"/>
						<i class="fa fa-search fa-lg fa-fw" aria-hidden="true"></i>
					</div>
				</form>
				<?php
					if (isset($_SESSION['username']))
					{
						$user=$_SESSION['username'];
						echo"
								<p> Welcome <em><strong>$user</strong></em> | <a href='page_admin/mainmenu.php'>Admin Panel</a> | <a onclick='return logout_user_frontend(\"$user\")' href='#'>Logout</a></p>
						";
					}
					else{
						echo"
								<p><a href='login.php'>Login</a> | <a href='register.php'>Register</a></p>
						";
						}
				?>
			</div>
		</div>
		<div class="bar_menu">
				<nav>
					<ul id="nav">
						<li><a href="index.php">Home</a></li>
						<li><a href="about.php">About</a></li>
						<li><a href="#">Type News</a>
							<ul>
								<li><a href="tech_index.php">Technology</a></li>
								<li><a href="finc_index.php">Finance</a></li>
								<li><a href="sport_index.php">Sport</a></li>
							</ul>
						</li>
					</ul>
				</nav>
		</div>
		<div id = "articlehome">
				<div class="hr_about" >
					<p>About Me</p>
					<hr width=30% size=7 NOSHADE>
				</div>
				<div class="about_page">
					<img src='assets/image/sttpln.png'>
					<table>
						<th width="36%" ></th>
						<th></th>
						<th></th>
						<tr>
							<td><p>Nama</p></td>
							<td><p>:&nbsp;</p></td>
							<td><p><em>Andika Prasatya</em></p></td>
						</tr>
						<tr>
							<td><p>Nim</p></td>
							<td><p>:&nbsp;</p></td>
							<td><p><em>201531220</em></p></td>
						</tr>
						<tr>
							<td><p>Email</p></td>
							<td><p>:&nbsp;</p></td>
							<td><p><em>Prasatya777@gmail.com</em></p></td>
						</tr>
						<tr>
							<td><p>Perguruan Tinggi</p></td>
							<td><p>:&nbsp;</p></td>
							<td><p><em>Sekolah Tinggi Teknik - PLN</em></p></td>
						</tr>
						<tr>
							<td><p>No Hp</p></td>
							<td><p>:&nbsp;</p></td>
							<td><p><em>081xxxxxxxxx</em></p></td>
						</tr>
					</table>
				</div>
			
			<div class = "kanan-articlehome about_clock">
				<div class="hr_home_kanan" >
					<p>Clock</p>
					<hr width=40% size=7 NOSHADE>
				</div>
				<div id="clock">
					<div class="clock">
					<div id="Date"></div>
						<ul>
							<li id="hours"> </li>
							<li id="point">:</li>
							<li id="min"> </li>
							<li id="point">:</li>
							<li id="sec"> </li>
						</ul>
					</div>
				</div>
			</div>

		</div>
		<div id = "footer">
			<div class="follow">
				<p>Follow Us</p>
			</div>
			<div class="middle">
				  <a class="btn" href="#">
					<i class="fab fa-facebook-f"></i>
				  </a>
				  <a class="btn" href="#">
					<i class="fab fa-twitter"></i>
				  </a>
				  <a class="btn" href="#">
					<i class="fab fa-google"></i>
				  </a>
				  <a class="btn" href="#">
					<i class="fab fa-instagram"></i>
				  </a>
				  <a class="btn" href="#">
					<i class="fab fa-youtube"></i>
				  </a>
			</div>
			<div class="copy_right">
				<p>Copy&copy;PrasatyaNews-201531220@sttpln.ac.id</p>
			</div>
		</div>
	</body>
</html>